 
================
Troubleshooting
================


Known issues:

* Hot keys may not work properly if the keyboard language is not English when the Arena software is loaded.
* Disconnections and interruptions may cause disconnection from the host level in the Arena.

