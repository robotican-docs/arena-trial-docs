 
================
Getting Started
================

.. contents:: Table of Contents
    :local:
    
Launching Arena
---------------

To launch Arena from terminal run:
    
    .. code-block:: console

        arena


To launch Arena and host specific level use the following syntax:
    
    .. code-block:: console

        arena -host=<user_name>,<level_name>,<latitude>,<longitude>,<altitude_amsl>,<time_of_day>,<use_oem_buildings>

for example:

    .. code-block:: console

        arena -host=john,CityPark,31.363789,34.877132,442.5,13,true


To Launch Arena in headless mode add the -RenderOffscreen argument, e.g.
    
    .. code-block:: console

        arena -RenderOffscreen -host=john,CityPark,31.363789,34.877132,442.5,13,true


See Host menu section for more details.


It is also possible to launch Arena from the grphical Application Launcher.


Menus
-----


Main menu
```````````

.. image:: images/mainMenu.png
   :align: center
   :width: 600

From the main menu you can set the user name that will be used for both hosting a new simulation and for joining an existing session.

Keep in mind that joining an existing session requires a unique user name.

It is also possible to set audio volume in the menu (bottom left slider).

Host menu
`````````````

.. image:: images/hostMenu.png
   :align: center
   :width: 600


This menu is used to select and set the level (envirenment) parameters.

The server name is used as identification name for multi user scenarios.

The geographic coordinates are used to set the absolute world position of the cartesian origin of the level.

Time of day is given as a decimal number between 0 to 24.

Join menu
`````````````

.. image:: images/joinMenu.png
   :align: center
   :width: 600


Entering this menu trigger a scan for available servers running ont the same LAN.

Clicking on a server name will initiate joining the server (*).

Hot Keys
--------
After hosting ir joining a server, you can use the following keys for exploring, debugging ir manually controlling the simulation:

.. list-table:: Hot Keys
   :widths: 25 100
   :header-rows: 1

   * - Key
     - Function
   * - F1
     - Toggle Hot Keys help view on screen
   * - Escape
     - Quit Arena
   * - Ctrl+q
     - Quit level (back to main menu)
   * - a,w,s,d,q,e
     - Camera view adjustment keys (can be used with mouse click and drag)
   * - k
     - Toggle Spawner widget (*)
   * - p
     - Toggle Profiling widget (*)
   * - o
     - Toggle Scalability widget (graphicas performance adjustments)
   * - t
     - Toggle Envirenment settings widget
   * - l
     - Toggle Robots widget (top left of the screen)
   * - n
     - Toggle robot head name view (some robots include additional properties view)
   * - v
     - Toggle Volume widget
   * - Arrows
     - Robots manual controls


(*) Not available in all trial versions



Widgets
--------

Scalability Widget
`````````````````````
.. image:: images/scalability.png
   :align: center
   :width: 500
|
Changing the slider will result in a change in the quality of the graphic engine and a change in the processing resources consumed.

Envirenment Settings Widget
`````````````````````````````
.. image:: images/enviorment.png
   :align: center
   :width: 500
|
Changing the Time of Day slider will change lightning conditions and sun position in the sky.


Volume Widget
`````````````````````````````
.. image:: images/volume.png
   :align: center
   :width: 500
|
Level audio effects volume slider

Robots Widget
``````````````````
.. image:: images/robots.png
   :align: center
   :width: 500

|
.. list-table:: Robots Widgets options (By clicking on a robot name)
   :widths: 1 1
   :header-rows: 1

   * - Option
     - Notes
   * - Left click
     - posseess the robot in its current location.
   * - Right click
     - destroy the robot.


Possessing a certain robot will result in the main camera tracking the robot and will also allow manual control if the robot allows such.
Please note, if the robot allows manual control, it will not be possible to control it through the ROS2 interface and the manual interface at the same time.