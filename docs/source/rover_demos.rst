 
================
Rover Demos
================

.. contents:: Table of Contents
    :local:



Waching the front camera video stream
-------------------------------------------

See Drone Demos page for video streaming and watching examples.

The Rover launch file is rover.launch.py and streamer port is 6000+instance.

For example, to launch Arena with a Rover robot, run:

    .. code-block:: console

        ros2 launch arena_trial_ros rover.launch.py arena:=true


To view the video stream using Gstreamer:

    .. code-block:: console

        gst-launch-1.0 -v tcpclientsrc host=127.0.0.1 port=6001 ! gdpdepay ! videoconvert ! autovideosink
        





Control Rover by publishing Twist messages
-------------------------------------------


#. Launch Arena + Rover:
    
    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true


#. Publish Twist messages:

    .. code-block:: console

        ros2 topic pub /Rover001/cmd_vel geometry_msgs/msg/Twist "{linear: {x: 1.0, y: 0.0, z: 0.0}, angular: {x: 0.0, y: 0.0, z: 0.8}}"

The above command drives the robot in forward velocity of 1 m/s and angular velocity of 0.8 rad/s. The robot will continue to follow this command untill new command will be snt.


Control Rover from rqt Robot Steering
-------------------------------------


#. Launch Arena + Rover:
    
    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true


#. launch rqt_robot_steering and set to topic at the top to "/Rover001/cmd_vel"
    
    .. code-block:: console

        ros2 run rqt_robot_steering rqt_robot_steering

Now you can play with the linear and angular velocity sliders.




Running the SLAM demo from the slam_toolbox package
------------------------------------------------------

#. Install `slam_toolbox <https://github.com/SteveMacenski/slam_toolbox>`_ for ROS2 Foxy:

.. code-block:: console

    sudo apt install ros-foxy-slam-toolbox

or for ROS2 Humble (NOT TESTED):

.. code-block:: console

    sudo apt install ros-humble-slam-toolbox



#. Launch Arena + Rover + RViz:

.. code-block:: console

    ros2 launch arena_trial_ros rover.launch.py arena:=true rviz:=true

In a new terminal run the SLAM launch file:

.. code-block:: console

    ros2 launch arena_trial_ros online_async_launch.py



Now you can possess and drive the rover and watch the map build on rviz.


.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="640" height="360" controls>
    <source src="_static/videos/slam.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>    
    </div>

For more details on SLAM implementation and configuration, see https://navigation.ros.org/tutorials/docs/navigation2_with_slam.html



