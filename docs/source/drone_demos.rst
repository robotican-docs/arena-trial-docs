 
==================
Drone Demos
==================

.. contents:: Table of Contents
    :local:

Control Drone with QGroundControl
-------------------------------------------
#. Download and install `QGroundControl <https://docs.qgroundcontrol.com/master/en/getting_started/download_and_install.html#ubuntu>`_.
#. Launch Arena + Drone:

    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true
#. Start QGroundControl and verify connection.
#. Send Takeoff command.
#. Send GoTo command.
#. Send Land caommand.

.. raw:: html
    
    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 1em;">
    <video width="640" height="360" controls>
    <source src="_static/videos/qgc.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>    
    </div>
|

Control Drone from code with MAVSDK
-------------------------------------------

#. Launch Arena + Drone:
    
    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true

#. In a new terminal, run the mavsdk offboard demo:
    
    .. code-block:: console

        ros2 run arena_trial_ros offboard udp://:14540


The demo code is not actually a ROS node, It is a modified version of the `MAVSDK offboard example. <https://github.com/mavlink/MAVSDK/blob/v1.4.16/examples/offboard/offboard.cpp>`_.

The 'udp://:14540' argument is the connection URL to to PX4 SITL. The UDP port is unique for each drone instance.


.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="640" height="360" controls>
    <source src="_static/videos/mavsdk.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>
    </div>


View Drone video stream with GStreamer
------------------------------------------------

#. Launch Arena + Drone:
    
    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true

#. Launch GStreamer view pipeline:

    .. code-block:: console

        gst-launch-1.0  tcpclientsrc host=127.0.0.1 port=5001 ! gdpdepay ! videoconvert ! autovideosink sync=false


You should see the video from the drone's stablaized camera:


.. image:: images/gstreamer.png
   :align: center
   :width: 600
|

Publish Drone video stream in ROS2 sensor_msgs/Image message
-----------------------------------------------------------------------

#. Install `gscam <https://github.com/ros-drivers/gscam.git>`_ and `image_view <https://github.com/ros-perception/image_pipeline/tree/foxy/image_view>`_ for ROS2 Foxy:

.. code-block:: console

    sudo apt install ros-foxy-gscam ros-foxy-image-view

or for ROS2 Humble:

.. code-block:: console

    sudo apt install ros-humble-gscam ros-humble-image-view

#. Launch Arena + Drone:
    
    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true


#. Wait for Arena to load and run gscam:
    
    .. code-block:: console

        GSCAM_CONFIG="tcpclientsrc host=127.0.0.1 port=5001 ! gdpdepay ! videoconvert" ros2 run gscam  gscam_node

  
#. To view to video, use image_view:
    
    .. code-block:: console

        ros2 run image_view image_view --ros-args --remap image:=/camera/image_raw -p autosize:=true

|

Publish Drone video stream in ROS2 using RTSP server 
--------------------------------------------------------

#. Launch Arena + Drone with RTSP proxy:

    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true rtsp_proxy:=true

#. Watch the video using GStreamer pipeline:

    .. code-block:: console

        gst-launch-1.0 rtspsrc location=rtsp://127.0.0.1:8555/video ! decodebin ! autovideosink sync=false
        


Control Drone Gimbal
------------------------------------------------


#. Run one of the above demos to launch and view the drone video stream.

#. Set the gimbal inertial position setpoint using the /Drone001/gimbal/command topic where x, y and z are roll, pith and azimuth:

Set Roll to 0.2 radians:

        .. code-block:: console

            ros2 topic pub /Drone001/gimbal/command geometry_msgs/msg/Vector3 "{x: 0.2, y: 0, z: 0}"

Set Pitch to -0.5 radians:

        .. code-block:: console

            ros2 topic pub /Drone001/gimbal/command geometry_msgs/msg/Vector3 "{x: 0.0, y: -0.5, z: 0}"


Set Azimuth to 0.5 radians:

        .. code-block:: console

            ros2 topic pub /Drone001/gimbal/command geometry_msgs/msg/Vector3 "{x: 0.0, y: 0.0, z: 0.6}"

Keep in mind that the gimbal behavior can be changed in the DroneBackend class. Currently the gimbal starts in FPV mode and changes to inertial position when command messages arrive.
It is also possible to control to gimbal in angular rate commands.


#. Viewing the gimbal actual state can be done using the /Drone001/gimbal/state topic:

    .. code-block:: console

        ros2 topic echo /Drone001/gimbal/state
        

Launch multiple Drones
--------------------------------

#. Launching multiple drones using a single launch file:

    .. code-block:: console

        ros2 launch arena_trial_ros drone_swarm.launch.py
        

#. Or you can launch multiple drones manually uding the drone.launch.py instance argument:

Terminal 1 - Launch Arena instace 1 by default:

    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py arena:=true

Terminal 2 - Launch instance 2:

    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py instance:=2

Terminal 3 - Launch instance 3:

    .. code-block:: console

        ros2 launch arena_trial_ros drone.launch.py instance:=3



.. image:: images/swarm.png
   :align: center
   :width: 640

|


* You can control the drones using QGroundContrl or by mavlink using UDP port: 14540+(instance-1), e.g. instance 2 port is 14541.

* Video streams are available on ports 5000+instance. e.g. instance 2 port is 5002.

* Initial conditions of each drone are specified in the config/drone.params.yaml file under the *simulation.scenario.drones* section.
