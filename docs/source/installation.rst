 
================
Installation
================

.. contents:: Table of Contents
    :local:
    
System Requirements
----------------------

* We support Ubuntu 20.04 and Ubuntu 22.04 on 64-bit x86.
* Nvidia graphics card with installed drivers is required.
* Minimum 16GB RAM
* Free disk storage > 6GB



Installing ROS2
------------------
For Ubuntu 20.04 please follow the official `ROS2 Foxy installation instructions  <https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html>`_.

For Ubuntu 22.04 please follow the official `ROS2 Humble installation instructions  <https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html>`_.


Installing Arena
-------------------------

#. Clone the Arena Trial repository using given username and deploy_token


    .. code-block:: console

        git clone https://<username>:<deploy_token>@gitlab.com/robotican/arena-simulation/arena-trial.git --depth 1

#. Source your ROS2 setup.bash

For ROS2 Foxy:

    .. code-block:: console

        source /opt/ros/foxy/setup.bash

or for ROS2 Humble:

    .. code-block:: console

        source /opt/ros/humble/setup.bash

#. Run installation script


    .. code-block:: console

        cd arena-trial
        ./install.sh


This script install required libraries and packages and the Arena software.

#. Setup licence by adding the following line to your ~/.bashrc file

    .. code-block:: console

        export ARENA_LICENSE_FILE_PATH=<PATH_TO_LICENCE_FILE>

where <PATH_TO_LICENCE_FILE> is the actual absolute path to your .bin license file


Installing Arena Trial ROS2 Package
---------------------------------------

#. Create new ROS2 workspace (or use an existing one)

    .. code-block:: console

        mkdir -p ~/arena_trial_ws/src

#. Clone arena_trial_ros package to your ROS2 workspace src folder (e.g ~/arena_trial_ws/src)

    .. code-block:: console

        cd  ~/arena_trial_ws/src
        git clone https://<username>:<deploy_token>@gitlab.com/robotican/arena-simulation/arena_trial_ros.git

#. Install Arena UAV backend, MAVSDK and PX4 SITL:

    .. code-block:: console

        cd ~/arena_trial_ws/src/arena_trial_ros/setup
        ./install.sh


#. Build workspace (make sure your ROS2 setup.bash is sourced in the current terminal before this step)

    .. code-block:: console

            cd ~/arena_trial_ws
            colcon build --symlink-install

#. Source the new workspace
    
    .. code-block:: console

        source ~/arena_trial_ws/install/setup.bash

Consider adding the above command to your ~/.bashrc to avoid running it manually on each new terminal.